﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Windows.Controls.Ribbon;
using MVVM.Model;

namespace FreeUML.Core.Plugins
{
    public class PluginManager
    {
        private const string CORE_PLUGINS = ".\\Core";
        private const string EXTERNAL_PLUGINS = ".\\Plugins";

        private Dictionary<Type, FreeUMLPlugin> _pluginInfos; 

        public PluginManager()
        {
            _pluginInfos = new Dictionary<Type, FreeUMLPlugin>();
        }

        public void LoadCorePlugins()
        {
            LoadPlugin(CORE_PLUGINS);
        }

        public void LoadExternalPlugins()
        {
            LoadPlugin(EXTERNAL_PLUGINS);
        }

        private void LoadPlugin(string path)
        {
            if (!Directory.Exists(path))
            {
                return;
            }

            string[] files = Directory.GetFiles(CORE_PLUGINS, "*.dll");

            foreach (string f in files)
            {
                try
                {
                    Assembly pluginAssembly = Assembly.LoadFrom(f);

                    foreach (Type t in pluginAssembly.GetTypes())
                    {
                        if ((typeof(IPlugin)).IsAssignableFrom(t))
                        {
                            foreach (Attribute attribute in t.GetCustomAttributes(typeof(FreeUMLPlugin)))
                            {
                                FreeUMLPlugin pInfo = attribute as FreeUMLPlugin;

                                _pluginInfos.Add(pInfo.InitializerType, pInfo);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(String.Format("Failed to load plug-in : {0}", path), ex);
                }
            }
        }

        public void BindPlugins()
        {
            
        }
    }
}
