﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeUML.Core.Plugins
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class FreeUMLPlugin : System.Attribute
    {
        public string PluginName { get; private set; }
        public Type InitializerType { get; private set; }
        public double Version { get; set; }
        public string Author { get; set; }
        public string UriLink { get; set; }
        public string Comment { get; set; }

        public FreeUMLPlugin(string pluginName, Type initializerType)
        {
            PluginName = pluginName;
            InitializerType = initializerType;
        }
    }
}
