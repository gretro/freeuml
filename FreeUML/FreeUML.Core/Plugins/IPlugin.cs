﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeUML.Core.Plugins
{
    public interface IPlugin
    {
        void RegisterPlugin(PluginManager manager);
    }
}
