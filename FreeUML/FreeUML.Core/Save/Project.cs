﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVVM.Model;

namespace FreeUML.Core.Save
{
    public class Project : ModelElement
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public bool ContainsUnsavedChanges { get; protected set; }

        public SaveTypes.SaveType SaveType { get; private set; }

        public Project(string path, SaveTypes.SaveType saveType)
        {
            Path = path;
            SaveType = saveType;
            Name = "Untitled FreeUML Project";
        }

        public void Save()
        {
            //TODO : Implement save strategy.


        }

        public static Project Load(string path)
        {
            //TODO : Implement Project loading.
            return null;
        }
    }
}
