﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeUML.Core.Save
{
    public static class SaveTypes
    {
        public static readonly SaveType SingleVersion = new SaveType("Single-versioned FreeUML Project", "sfu");
        public static readonly SaveType MultiVersion = new SaveType("Multi-versioned FreeUML Project", "mfu");
        

        public static readonly SaveType[] AvailableSaveTypes = new SaveType[]
                                                               {
                                                                   SingleVersion,
                                                                   MultiVersion
                                                               }; 

        public static string GetDialogFilterString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < AvailableSaveTypes.Length; i++)
            {
                sb.Append(AvailableSaveTypes[i].ToString());

                if (i < AvailableSaveTypes.Length - 1)
                {
                    sb.Append("|");
                }
            }

            return sb.ToString();
        }

        public static SaveType GetSaveType(string extension)
        {
            return
                AvailableSaveTypes.FirstOrDefault(
                    st => st.Extension.Equals(extension, StringComparison.InvariantCultureIgnoreCase));
        }

        public class SaveType
        {
            public string TypeName { get; private set; }
            public string Extension { get; private set; }

            internal SaveType(string typeName, string extension)
            {
                TypeName = typeName;
                Extension = extension;
            }

            public override string ToString()
            {
                return string.Format("{0}|.{1}", TypeName, Extension);
            }
        }
    }
}
