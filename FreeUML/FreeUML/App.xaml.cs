﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using FreeUML.Core.Plugins;
using FreeUML.ErrorHandling;

namespace FreeUML
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Dispatcher.UnhandledException += DispatcherOnUnhandledException;
        }

        private void DispatcherOnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ErrorDialog eDialog = new ErrorDialog();
            eDialog.DataContext = new ErrorModel(e.Exception);

            eDialog.ShowDialog();
            e.Handled = true;

            Current.Shutdown(-1);
        }
    }
}
