﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FreeUML.Base;

namespace FreeUML
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window
    {
        public LoadingWindow()
        {
            InitializeComponent();
            DataContext = new FreeUMLVM();

            FreeUMLModel.Instance.ModelUpdated += ModelUpdated;
            FreeUMLModel.Instance.LoadPluginsAsync();
        }

        private void ModelUpdated(object sender, EventArgs eventArgs)
        {
            if (FreeUMLModel.Instance.HasFinishedLoading)
            {
                FreeUMLModel.Instance.ModelUpdated -= ModelUpdated;
                LoadWindow();
            }
        }

        private void LoadWindow()
        {
            MainWindow mWindow = new MainWindow();
            mWindow.Show();

            this.Close();
        }
    }
}
