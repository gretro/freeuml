﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FreeUML.Base;
using Microsoft.Windows.Controls.Ribbon;

namespace FreeUML
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FreeUMLVM _vm;

        public MainWindow()
        {
            InitializeComponent();

            _vm = new FreeUMLVM();
            DataContext = _vm;
            
            Closing += _vm.ProjectClosing;
            Closed += OnClosed;
        }

        private void OnClosed(object sender, EventArgs eventArgs)
        {
            Application.Current.Shutdown();
        }
    }
}
