﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVVM.Model;

namespace FreeUML.ErrorHandling
{
    public class ErrorModel : ModelElement
    {
        public string CallStack { get; set; }

        public ErrorModel(Exception e)
        {
            CallStack = e.StackTrace;
        }
    }
}
