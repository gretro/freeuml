﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using FreeUML.Core.Plugins;
using FreeUML.Core.Save;
using MVVM.Model;

namespace FreeUML.Base
{
    public sealed class FreeUMLModel : ModelElement
    {
        private static FreeUMLModel _instance;

        public static FreeUMLModel Instance
        {
            get
            {
                return _instance ?? (_instance = new FreeUMLModel());
            }
        }

        public event CancelEventHandler ProjectClosing;

        private string _version;
        private PluginManager _pluginManager;

        public string Version
        {
            get
            {
                if (_version == null)
                {
                    Version v = Assembly.GetEntryAssembly().GetName().Version;
                    _version = String.Format("{0}.{1}", v.Major, v.Minor);

                    if (v.Revision != 0)
                    {
                        _version = _version + String.Format(".{0}", v.Revision);
                    }
                }

                return _version;
            }
        }

        public Project LoadedProject { get; private set; }

        public string StatusMessage { get; private set; }
        public bool HasFinishedLoading { get; private set; }

        private FreeUMLModel()
        {
            StatusMessage = "Loading...";
            HasFinishedLoading = false;
        }

        public void LoadPluginsAsync()
        {
            if (HasFinishedLoading || _pluginManager != null)
            {
                throw new InvalidOperationException("Cannot load plugins more than once.");
            }

            _pluginManager = new PluginManager();

            Thread loadingThread = new Thread(LoadPlugins);
            loadingThread.Start();
        }

        private void LoadPlugins()
        {
            StatusMessage = "Loading Core plugins...";
            Application.Current.Dispatcher.Invoke(OnModelUpdated);
            _pluginManager.LoadCorePlugins();

            Thread.Sleep(500);

            StatusMessage = "Loading external plugins...";
            Application.Current.Dispatcher.Invoke(OnModelUpdated);
            _pluginManager.LoadExternalPlugins();

            Thread.Sleep(500);

            StatusMessage = "Starting FreeUML...";
            HasFinishedLoading = true;
            Application.Current.Dispatcher.Invoke(OnModelUpdated);
        }

        /// <summary>
        /// Is called when the project is about to be closed.
        /// </summary>
        /// <param name="e">Event Argument allowing event to be cancelled.</param>
        /// <returns></returns>
        private bool OnProjectClosing(CancelEventArgs e)
        {
            if (ProjectClosing != null)
            {
                foreach (CancelEventHandler del in ProjectClosing.GetInvocationList())
                {
                    del.Invoke(this, e);

                    if (e.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void CreateNewProject(string path, SaveTypes.SaveType saveType)
        {
            if (!OnProjectClosing(new CancelEventArgs()))
            {
                return;
            }

            LoadedProject = new Project(path, saveType);
        }
    }
}
