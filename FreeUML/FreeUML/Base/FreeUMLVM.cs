﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using FreeUML.Core.Save;
using Microsoft.Win32;
using MVVM;
using MVVM.UI;

namespace FreeUML.Base
{
    public class FreeUMLVM : ViewModel<FreeUMLModel>
    {
        public string Version
        {
            get
            {
                return Model.Version;
            }
        }

        public string StatusMessage
        {
            get
            {
                return Model.StatusMessage;
            }
        }

        public bool HasFinishedLoading
        {
            get
            {
                return Model.HasFinishedLoading;
            }
        }

        public FreeUMLVM()
            :base(FreeUMLModel.Instance)
        {
            CommandManager.RegisterClassCommandBinding(typeof(Window), new CommandBinding(ApplicationCommands.New, NewCommand_Executed));
            CommandManager.RegisterClassCommandBinding(typeof(Window), new CommandBinding(ApplicationCommands.Open, OpenCommand_Executed));
            CommandManager.RegisterClassCommandBinding(typeof(Window), new CommandBinding(ApplicationCommands.Close, CloseCommand_Executed, 
                CloseCommand_CanExecute));

            FreeUMLModel.Instance.ProjectClosing += ProjectClosing;
        }

        public void ProjectClosing(object sender, CancelEventArgs e)
        {
            if (FreeUMLModel.Instance.LoadedProject != null)
            {
                MessageBoxResult r = MessageBox.Show(new Window(), "There are unsaved changes in the opened project. Would you like to save them ?",
                    "Unsaved changes", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                e.Cancel = (r == MessageBoxResult.Cancel);

                if (r == MessageBoxResult.Yes)
                {
                    FreeUMLModel.Instance.LoadedProject.Save();
                }
            }
        }

        public override void Refresh()
        {
            base.Refresh();

            OnPropertyChanged(()=>StatusMessage);
            OnPropertyChanged(()=>HasFinishedLoading);
        }

        private void NewCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "New FreeUML project";

            sfd.FileName = "UntitledProject1.sfu";
            sfd.AddExtension = true;
            sfd.CreatePrompt = false;
            sfd.OverwritePrompt = true;
            sfd.Filter = SaveTypes.GetDialogFilterString();

            bool? result = sfd.ShowDialog();

            if (result.Value)
            {
                string path = sfd.FileName;
                SaveTypes.SaveType saveType = SaveTypes.GetSaveType(path.Split('.').LastOrDefault());

                FreeUMLModel.Instance.CreateNewProject(path, saveType);
            }
        }

        private void OpenCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }

        private void CloseCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }
    }
}
