﻿using System;

namespace MVVM.Model
{
    /// <summary>
    /// Base class of a model.
    /// </summary>
    public abstract class ModelElement
    {
        public event EventHandler ModelUpdating;
        public event EventHandler ModelUpdated;

        /// <summary>
        /// Called when a model is starting to update.
        /// </summary>
        protected void OnModelUpdating()
        {
            if (ModelUpdating != null)
            {
                ModelUpdating.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Called when a model has finished updating.
        /// </summary>
        protected void OnModelUpdated()
        {
            if (ModelUpdated != null)
            {
                ModelUpdated.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Updates the model.
        /// </summary>
        /// <param name="notify">If true, will notify the </param>
        public void UpdateModel(bool notify = true)
        {
            if (notify)
            {
                OnModelUpdating();
            }

            Update();

            if (notify)
            {
                OnModelUpdated();
            }
        }

        /// <summary>
        /// Updates the model.
        /// </summary>
        /// <remarks>Useful in case the model is making any calculations.</remarks>
        protected virtual void Update()
        {
            
        }
    }
}
