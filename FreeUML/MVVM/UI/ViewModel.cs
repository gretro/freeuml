﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using MVVM.Annotations;
using MVVM.Model;

namespace MVVM.UI
{
    /// <summary>
    /// Base class of a ViewModel. Provides basics to update UI.
    /// </summary>
    public abstract class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notifies the UI the specified property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <example>OnPropertyChanged("Property");</example>
        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Notifies the UI the specified property has changed.
        /// </summary>
        /// <param name="property">Expression refering to a property.</param>
        /// <example>OnPropertyChanged(()=>Property);</example>
        protected void OnPropertyChanged<T>(Expression<Func<T>>  property)
        {
            OnPropertyChanged((property.Body as MemberExpression).Member.Name);
        }

        /// <summary>
        /// Manually refreshes the UI after the model has been updated.
        /// </summary>
        public virtual void Refresh()
        {
            
        }
    }

    /// <summary>
    /// Typed base class of a ViewModel. Provides basics to update UI.
    /// </summary>
    /// <typeparam name="T">Model type</typeparam>
    public abstract class ViewModel<T> : ViewModel
        where T : ModelElement
    {
        private T _model;

        /// <summary>
        /// Model registered to be used with the ViewModel. Will register the model
        /// so the Refresh() or Refresh(T model) method will be called when the model updates.
        /// </summary>
        public T Model
        {
            get
            {
                return _model;
            }
            set
            {
                if (_model != null)
                {
                    _model.ModelUpdated -= ModelUpdated;
                }

                _model = value;

                if (_model != null)
                {
                    _model.ModelUpdated += ModelUpdated;
                }
            }
        }

        /// <summary>
        /// Constructor for a typed ViewModel with a model only known at update time (like a SubVM).
        /// </summary>
        protected ViewModel()
            :this(null)
        {
            
        }

        /// <summary>
        /// Constructor for a typed ViewModel with a fixed model.
        /// </summary>
        /// <param name="model">Model to be used in the ViewModel.</param>
        protected ViewModel(T model)
        {
            Model = model;

            InitializeViewModel();

            Refresh(Model);
        }

        private void ModelUpdated(object sender, EventArgs eventArgs)
        {
            Refresh(Model);
        }

        /// <summary>
        /// Initializes the ViewModel.
        /// </summary>
        protected virtual void InitializeViewModel()
        {
            
        }

        /// <summary>
        /// Manually refreshes the UI after the model has been updated.
        /// </summary>
        /// <param name="model">Model used to update the VM and the UI.</param>
        /// <remarks>Useful for Sub-VMs. Override with no base.Refresh(model);</remarks>
        public virtual void Refresh(T model)
        {
            Refresh();
        }
    }
}
