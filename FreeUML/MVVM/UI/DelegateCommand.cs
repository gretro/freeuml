﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MVVM.Annotations;

namespace MVVM
{
    public class DelegateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private bool? _canExecuteCache;

        public Action ExecuteCommand { get; private set; }
        public Func<bool> CanExecuteCommand { get; private set; }

        public DelegateCommand(Action execute, Func<bool> canExecute = null)
        {
            ExecuteCommand = execute;
            CanExecuteCommand = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecuteCache == null)
            {
                RefreshCanExecuteCache();
            }

            return _canExecuteCache ?? true;
        }

        public virtual void Execute(object parameter)
        {
            if (ExecuteCommand != null)
            {
                ExecuteCommand.Invoke();
            }
        }

        /// <summary>
        /// Rafraîchie la commande pour s'assurer qu'elle puisse être exécutée.
        /// </summary>
        public void Refresh()
        {
            RefreshCanExecuteCache();

            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, new EventArgs());
            }
        }

        private void RefreshCanExecuteCache()
        {
            _canExecuteCache = CanExecuteCommand == null || CanExecuteCommand.Invoke();
        }
    }

    public class DelegateCommand<T> : DelegateCommand
    {
        public new Action<T> ExecuteCommand { get; private set; }

        public DelegateCommand(Action<T> execute, Func<bool> canExecute = null)
            : base(null, canExecute)
        {
            ExecuteCommand = execute;
        }

        public override void Execute(object parameter)
        {
            if (ExecuteCommand != null)
            {
                ExecuteCommand.Invoke((T)parameter);
            }
        }
    }
}
