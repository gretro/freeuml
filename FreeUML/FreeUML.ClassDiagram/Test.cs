﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeUML.Core.Plugins;
using MVVM.Model;

namespace FreeUML.ClassDiagram
{
    [FreeUMLPlugin("Test Plug-In", typeof(Test), Author = "Gabriel Lemire", Version = 0.1, UriLink = "http://www.google.com")]
    public class Test : IPlugin
    {
        public void RegisterPlugin(PluginManager manager)
        {
            
        }
    }
}
